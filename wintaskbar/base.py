from enum import Enum

class TaskbarState(Enum):
    NoProgress = 0x00
    Indeterminate = 0x01
    Normal = 0x02
    Error = 0x04
    Paused = 0x08

def wx_top_level_parent(window) -> int:
    """
        wxpython method that returns the top level parent's handle
        of the current wx object. Use this when creating a progress
        bar where the parent would is not the top level parent.
    """
    return window.GetTopLevelParent().Handle

class TaskbarBase(object):
    def __init__(self, parent, value=0, range=100, handle_func=None):
        """
            Creates an application taskbar object.

            :param parent: top level object for the applcation
            :param value: starting value for the taskbar progress bar
            :param range: starting maximum value for the taskbar progress bar
            :param handle_func: custom method for getting the handle from the parent object (defaults to parent.Handle for wxpython)
        """

        self._parent = parent
        self._state = TaskbarState.NoProgress
        self._activated = False
        self._range = range
        self._value = value
        self._get_handle = handle_func or (lambda f: f.Handle)

    @property
    def Range(self) -> int:
        return self._range

    @Range.setter
    def Range(self, value: int):
        value = int(value)
        if value != self._range:
            self._range = value
            self._progress()

    def SetRange(self, value):
        self.Range = value
    def GetRange(self):
        return self.Range

    @property
    def Value(self) -> int:
        return self._value

    @Value.setter
    def Value(self, value: int):
        value = int(value)
        if self._value != value:
            self._value = value
            self._progress()
            self.State = TaskbarState.Normal

    def SetValue(self, value):
        self.Value = value

    def GetValue(self):
        return self.Value

    @property
    def State(self):
        return self._state

    @State.setter
    def State(self, value):
        self._state = value
        self._set_state()

    def SetState(self, value):
        self.State = value

    def GetState(self):
        return self.State

    def Pulse(self):
        """
            Changes the progress bar to indeterminate state,
            GetValue() and GetRange() will no longer map to the actual values.
        """
        self.State = TaskbarState.Indeterminate

    def _set_state(self):
        pass
    def _progress(self):
        pass

    @property
    def _handle(self):
        return self._get_handle(self._parent)
