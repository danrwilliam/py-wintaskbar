import os
from wintaskbar.base import TaskbarState, wx_top_level_parent

if os.name == 'nt':
    from wintaskbar.win import TaskbarWin as Taskbar
elif os.name == 'posix':
    try:
        from gtk import TaskbarGtk as Taskbar
    except:
        from wintaskbar.base import TaskbarBase as Taskbar
