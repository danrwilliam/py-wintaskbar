from setuptools import setup, find_packages

setup(
    name='wintaskbar',
    version='0.3',
    packages=find_packages(),
    install_requires=[
        'comtypes==1.1.7',
    ],
    package_data={
        'wintaskbar': ['taskbarlib.tlb']
    },
    include_package_data=True
)
