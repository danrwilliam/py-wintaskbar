from wintaskbar.base import TaskbarBase, TaskbarState
import os
tlb_path = os.path.join(os.path.dirname(__file__), "TaskbarLib.tlb")
import comtypes.client as cc
cc.GetModule(tlb_path)
import comtypes.gen.TaskbarLib as tbl

class TaskbarWin(TaskbarBase):
    def __init__(self, parent, value=0, range=100, handle_func=None):
        """
            Creates an application taskbar object.

            :param parent: top level object for the application
            :param value: starting value for the taskbar progress bar
            :param range: starting maximum value for the taskbar progress bar
            :param handle_func: custom method for getting the handle from the parent object (defaults to parent.Handle for wxpython)
        """

        self._bar = cc.CreateObject("{56FDF344-FD6D-11d0-958A-006097C9A090}", interface=tbl.ITaskbarList3)
        super().__init__(parent, value, range, handle_func)

    def _activate_if_needed(self):
        if not self._activated and self._handle is not None:
            self._activated = True
            self._bar.HrInit()
            self._bar.ActivateTab(self._handle)
        return self._activated

    def _progress(self):
        if self._activate_if_needed():
            self._bar.SetProgressValue(self._handle, self._value, self._range)

    def _set_state(self):
        if self._activate_if_needed():
            self._bar.SetProgressState(self._handle, self._state.value)
